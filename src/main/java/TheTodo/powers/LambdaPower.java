package TheTodo.powers;

import com.megacrit.cardcrawl.core.AbstractCreature;

public abstract class LambdaPower extends BasePower {

    public LambdaPower(String name, PowerType power_type, boolean turn_based, AbstractCreature owner, int amount) {
        super(name, power_type, turn_based, owner, amount);
    }

    public LambdaPower(String name, PowerType power_type, boolean turn_based, AbstractCreature owner,
                     int amount, boolean is_amount_percent) {
        super(name, power_type, turn_based, owner, amount, is_amount_percent);
    }

    public LambdaPower(String name, PowerType power_type, boolean turn_based, AbstractCreature owner,
                     int amount, int amount2) {
        super(name, power_type, turn_based, owner, amount, amount2);
    }

    public LambdaPower(String name, PowerType power_type, boolean turn_based, AbstractCreature owner,
                     int amount, boolean is_amount_percent,
                     int amount2, boolean is_amount2_percent) {
        super(name, power_type, turn_based, owner, amount, is_amount_percent, amount2, is_amount2_percent);
    }

    public abstract void updateDescription();
}
