package TheTodo.cards.examples.complex;

import TheTodo.cards.BaseCard;
import basemod.cardmods.EtherealMod;
import basemod.cardmods.ExhaustMod;
import basemod.helpers.CardModifierManager;
import com.evacipated.cardcrawl.mod.stslib.actions.common.SelectCardsAction;
import com.megacrit.cardcrawl.actions.common.MakeTempCardInHandAction;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;

import java.util.ArrayList;
import java.util.Collections;

import static TheTodo.TodoMod.id;
import static TheTodo.util.Wiz.atb;
import static TheTodo.util.Wiz.att;
import static TheTodo.util.Wiz.cards;

public class SelectCardsAndCardModsDemo extends BaseCard {
    public final static String NAME = SelectCardsAndCardModsDemo.class.getSimpleName();
    public final static String ID = id(NAME);
    public final static CardType TYPE = CardType.SKILL;
    public final static CardTarget TARGET = CardTarget.SELF;
    public final static CardRarity RARITY = CardRarity.UNCOMMON;

    public final static int COST = 1;
    public final static int UP_COST = 0;

    public SelectCardsAndCardModsDemo() {
        super(NAME, COST, TYPE, RARITY, TARGET);
    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        ArrayList<AbstractCard> choices = new ArrayList<>();
        ArrayList<AbstractCard> valid_cards = cards(c -> c.cost == 0, true);
        Collections.shuffle(valid_cards);

        for (int i = 0; i < 3; i++) {
            CardModifierManager.addModifier(valid_cards.get(i), new EtherealMod());
            CardModifierManager.addModifier(valid_cards.get(i), new ExhaustMod());
            choices.add(valid_cards.get(i));
        }

        atb(new SelectCardsAction(choices, 1, CARD_STRINGS.EXTENDED_DESCRIPTION[0],
                (cards) -> att(new MakeTempCardInHandAction(cards.get(0), 1, true))));
    }

    @Override
    public void upp() {
        upgradeBaseCost(UP_COST);
    }
}