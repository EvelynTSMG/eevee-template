package TheTodo.cards.examples.complex;

import TheTodo.cards.BaseCard;
import TheTodo.util.Wiz;
import basemod.patches.com.megacrit.cardcrawl.cards.AbstractCard.MultiCardPreview;
import com.megacrit.cardcrawl.actions.AbstractGameAction;
import com.megacrit.cardcrawl.actions.common.DrawCardAction;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.cards.tempCards.Safety;
import com.megacrit.cardcrawl.cards.tempCards.Smite;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;

import static TheTodo.TodoMod.id;
import static TheTodo.util.Wiz.atb;

public class MultiCardPreviewAndDrawCallbackDemo extends BaseCard {
    public final static String NAME = MultiCardPreviewAndDrawCallbackDemo.class.getSimpleName();
    public final static String ID = id(NAME);
    public final static CardType TYPE = CardType.ATTACK;
    public final static CardTarget TARGET = CardTarget.ALL_ENEMY;
    public final static CardRarity RARITY = CardRarity.UNCOMMON;

    public final static int COST = 1;
    public final static int DMG = 10;
    public final static int UP_DMG = 1;

    public MultiCardPreviewAndDrawCallbackDemo() {
        super(NAME, COST, TYPE, RARITY, TARGET);
        dmg(DMG);
        // Add Smite and Safety to the preview that appears when hovering over this card
        MultiCardPreview.add(this, new Smite(), new Safety());
    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        // Create new Smite and Safety cards
        AbstractCard q = new Smite();
        AbstractCard q2 = new Safety();

        // If this card is upgraded, upgrade them
        if (upgraded) {
            q.upgrade();
            q2.upgrade();
        }

        // Shuffle them into the draw pile
        Wiz.make_card_shuffle(q);
        Wiz.make_card_shuffle(q2);

        // Draw 1 card, then immediately after do another action
        atb(new DrawCardAction(1, new AbstractGameAction() {
            @Override
            public void update() {
                isDone = true; // Say we're done immediately, and
                // check if any of the drawn cards were colorless
                if (DrawCardAction.drawnCards.stream().anyMatch(q -> q.color.equals(CardColor.COLORLESS))) {
                    // If so, damage all enemies
                    damage_all_now(AttackEffect.SLASH_VERTICAL);
                }
            }
        }));
    }

    public void upp() {
        upgradeDamage(UP_DMG);

        // Replace the previous previews with new, upgraded cards
        AbstractCard q = new Smite();
        AbstractCard q2 = new Safety();
        q.upgrade();
        q2.upgrade();

        MultiCardPreview.clear(this); // Clear the old ones
        MultiCardPreview.add(this, q, q2); // Add the new ones

        updateDescription();
    }
}