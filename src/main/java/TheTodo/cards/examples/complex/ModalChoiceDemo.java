package TheTodo.cards.examples.complex;

import TheTodo.actions.ModalChoiceAction;
import TheTodo.cards.BaseCard;
import TheTodo.cards.ModalChoiceCard;
import com.megacrit.cardcrawl.actions.common.DrawCardAction;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import com.megacrit.cardcrawl.powers.StrengthPower;

import java.util.ArrayList;

import static TheTodo.TodoMod.id;
import static TheTodo.util.Wiz.apply_self_now;
import static TheTodo.util.Wiz.atb;
import static TheTodo.util.Wiz.att;

public class ModalChoiceDemo extends BaseCard {
    public final static String NAME = ModalChoiceDemo.class.getSimpleName();
    public final static String ID = id(NAME);
    public final static CardType TYPE = CardType.SKILL;
    public final static CardTarget TARGET = CardTarget.SELF;
    public final static CardRarity RARITY = CardRarity.UNCOMMON;

    public final static int COST = 1;
    public final static int MAGIC = 3;
    public final static int UP_MAGIC = 1;
    public final static int MAGIC2 = 1;
    public final static int UP_MAGIC2 = 1;

    public ModalChoiceDemo() {
        super(NAME, COST, TYPE, RARITY, TARGET);
        magic(MAGIC);
        magic2(MAGIC2);
    }

    public void use(AbstractPlayer p, AbstractMonster m) {
        ArrayList<AbstractCard> cards = new ArrayList<>();

        // Create a card named "Draw" with the description "Draw {magicNumber} cards."
        cards.add(new ModalChoiceCard(
                CARD_STRINGS.EXTENDED_DESCRIPTION[0],
                String.format(CARD_STRINGS.EXTENDED_DESCRIPTION[1], magicNumber),
                () -> att(new DrawCardAction(magicNumber))));

        // Create a card named "Strength" with the description "Gain {magic2} Strength."
        cards.add(new ModalChoiceCard(
                CARD_STRINGS.EXTENDED_DESCRIPTION[2],
                String.format(CARD_STRINGS.EXTENDED_DESCRIPTION[3], magic2),
                () -> apply_self_now(new StrengthPower(p, magic2))));

        // Offer the player a choice between them
        atb(new ModalChoiceAction(cards));
    }

    public void upp() {
        upgradeMagicNumber(UP_MAGIC);
        upgradeMagic2(UP_MAGIC2);
    }
}