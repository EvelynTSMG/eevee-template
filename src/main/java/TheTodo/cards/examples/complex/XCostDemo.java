package TheTodo.cards.examples.complex;

import TheTodo.actions.XCostAction;
import TheTodo.cards.BaseCard;
import com.megacrit.cardcrawl.actions.AbstractGameAction;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import com.megacrit.cardcrawl.powers.StrengthPower;

import static TheTodo.TodoMod.id;
import static TheTodo.util.Wiz.apply_self_now;
import static TheTodo.util.Wiz.atb;

public class XCostDemo extends BaseCard {
    public final static String NAME = XCostDemo.class.getSimpleName();
    public final static String ID = id(NAME);
    public final static CardType TYPE = CardType.ATTACK;
    public final static CardTarget TARGET = CardTarget.ENEMY;
    public final static CardRarity RARITY = CardRarity.RARE;

    public final static int COST = -1;
    public final static int DMG = 5;
    public final static int MAGIC = 0;
    public final static int UP_MAGIC = 1;

    public XCostDemo() {
        super(NAME, COST, TYPE, RARITY, TARGET);
        dmg(DMG);
        magic(MAGIC);
        exhaust = true;
    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        atb(new XCostAction(this, (effect, params) -> {
            for (int i = 0; i < effect + params[0]; i++) {
                damage_now(m, AbstractGameAction.AttackEffect.SLASH_DIAGONAL);
            }
            apply_self_now(new StrengthPower(p, effect + params[0]));
            return true;
        }, magicNumber));
    }

    @Override
    public void upp() {
        upgradeMagicNumber(UP_MAGIC);
        updateDescription();
    }
}