package TheTodo.cards.examples.complex;

import TheTodo.cards.BaseCard;
import TheTodo.powers.LambdaPower;
import com.megacrit.cardcrawl.actions.common.GainBlockAction;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.cards.DamageInfo;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import com.megacrit.cardcrawl.powers.AbstractPower;

import static TheTodo.TodoMod.id;
import static TheTodo.util.Wiz.apply_self;
import static TheTodo.util.Wiz.atb;

public class InlinePowerDemo extends BaseCard {
    public final static String NAME = InlinePowerDemo.class.getSimpleName();
    public final static String ID = id(NAME);
    public final static String POWER_NAME = "StrikeAndBlockBoostPower";
    public final static CardType TYPE = CardType.POWER;
    public final static CardTarget TARGET = CardTarget.SELF;
    public final static CardRarity RARITY = CardRarity.UNCOMMON;

    public final static int COST = 1;
    public final static int MAGIC = 4;
    public final static int UP_MAGIC = 2;

    public InlinePowerDemo() {
        super(NAME, COST, TYPE, RARITY, TARGET);
        magic(MAGIC);
    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        apply_self(new LambdaPower(POWER_NAME, AbstractPower.PowerType.BUFF, false, p, magicNumber) {
            @Override
            public float atDamageGive(float damage, DamageInfo.DamageType type, AbstractCard card) {
                if (card.hasTag(CardTags.STRIKE)) {
                    return damage + amount;
                }
                return damage;
            }

            @Override
            public void atEndOfTurnPreEndTurnCards(boolean isPlayer) {
                flash();
                atb(new GainBlockAction(owner, amount));
            }

            @Override
            public void updateDescription() {
                // hacky code to get our desired name since it's otherwise automatic
                name = CARD_STRINGS.EXTENDED_DESCRIPTION[0];
                description = String.format(CARD_STRINGS.EXTENDED_DESCRIPTION[1], amount, amount);
            }
        });
    }

    @Override
    public void upp() {
        upgradeMagicNumber(UP_MAGIC);
    }
}