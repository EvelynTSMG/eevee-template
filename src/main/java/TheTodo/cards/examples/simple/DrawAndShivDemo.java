package TheTodo.cards.examples.simple;

import TheTodo.cards.BaseCard;
import com.megacrit.cardcrawl.actions.common.DrawCardAction;
import com.megacrit.cardcrawl.cards.tempCards.Shiv;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;

import static TheTodo.TodoMod.id;
import static TheTodo.util.Wiz.atb;
import static TheTodo.util.Wiz.make_card_hand;

public class DrawAndShivDemo extends BaseCard {
    public final static String NAME = DrawAndShivDemo.class.getSimpleName();
    public final static String ID = id(NAME);
    public final static CardType TYPE = CardType.SKILL;
    public final static CardTarget TARGET = CardTarget.SELF;
    public final static CardRarity RARITY = CardRarity.UNCOMMON;

    public final static int COST = 1;
    public final static int UP_COST = 0;
    public final static int MAGIC = 1;

    public DrawAndShivDemo() {
        super(NAME, COST, TYPE, RARITY, TARGET);
        magic(MAGIC);
    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        // Add to the bottom of the action queue an action which draws magicNumber card(s).
        atb(new DrawCardAction(magicNumber));
        // Create a Shiv in your hand.
        make_card_hand(new Shiv());
    }

    @Override
    public void upp() {
        upgradeBaseCost(UP_COST);
    }
}