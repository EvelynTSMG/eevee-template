package TheTodo.cards.examples.simple;

import TheTodo.cards.BaseCard;
import com.evacipated.cardcrawl.mod.stslib.cards.interfaces.StartupCard;
import com.megacrit.cardcrawl.actions.AbstractGameAction;
import com.megacrit.cardcrawl.actions.common.GainBlockAction;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.monsters.AbstractMonster;

import static TheTodo.TodoMod.id;
import static TheTodo.util.Wiz.atb;

public class StartupBlockDemo extends BaseCard
        // We implement StartupCard to allow us to do pre-battle effects
        implements StartupCard {
    public final static String NAME = StartupBlockDemo.class.getSimpleName();
    public final static String ID = id(NAME);
    public final static CardType TYPE = CardType.SKILL;
    public final static CardTarget TARGET = CardTarget.SELF;
    public final static CardRarity RARITY = CardRarity.UNCOMMON;

    public final static int COST = 1;
    public final static int DMG = 7;
    public final static int UP_DMG = 2;
    public final static int MAGIC = 4;
    public final static int UP_MAGIC = 1;

    public StartupBlockDemo() {
        super(NAME, COST, TYPE, RARITY, TARGET);
        dmg(DMG);
        magic(MAGIC);
        // This card attacks ALL enemies, so isMultiDamage is true,
        isMultiDamage = true;
    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        // and damage_all is used instead of damage
        damage_all(AbstractGameAction.AttackEffect.FIRE);
    }

    @Override
    public boolean atBattleStartPreDraw() {
        // Since this isn't a card being played, we use a flat value (`magicNumber`) for the Block.
        // This was a decision made by DarkVexon. I don't know how I feel about it q:
        atb(new GainBlockAction(AbstractDungeon.player, magicNumber));

        // The return value is if you want this card to show up pre-fight, to indicate the effect triggered.
        // Since it always triggers, we always return true.
        return true;
    }

    @Override
    public void upp() {
        upgradeDamage(UP_DMG);
        upgradeMagicNumber(UP_MAGIC);
    }
}