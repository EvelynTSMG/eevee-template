package TheTodo.cards.examples.simple;

import TheTodo.cards.BaseCard;
import com.megacrit.cardcrawl.actions.AbstractGameAction;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import com.megacrit.cardcrawl.powers.VulnerablePower;

import static TheTodo.TodoMod.id;

public class TwoTypesOfDamageDemo extends BaseCard {
    public final static String NAME = TwoTypesOfDamageDemo.class.getSimpleName();
    public final static String ID = id(NAME);
    public final static CardType TYPE = CardType.ATTACK;
    public final static CardTarget TARGET = CardTarget.ENEMY;
    public final static CardRarity RARITY = CardRarity.UNCOMMON;

    public final static int COST = 1;
    public final static int DMG = 8;
    public final static int UP_DMG = 2;
    public final static int DMG2 = 15;
    public final static int UP_DMG2 = 5;

    public TwoTypesOfDamageDemo() {
        super(NAME, COST, TYPE, RARITY, TARGET);
        dmg(DMG);
        dmg2(DMG2);
    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        if (p.hasPower(VulnerablePower.POWER_ID)) { // If you have VulnerablePower (vulnerable),
            damage2(m, AbstractGameAction.AttackEffect.BLUNT_HEAVY); // Deal damage based on secondDamage.
        } else {
            damage(m, AbstractGameAction.AttackEffect.BLUNT_LIGHT); // Otherwise deal normal damage.
        }
    }

    @Override
    public void upp() {
        upgradeDamage(UP_DMG);
        upgradeDamage2(UP_DMG2);
    }
}