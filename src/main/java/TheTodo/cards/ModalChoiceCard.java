package TheTodo.cards;

import basemod.AutoAdd;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;

@AutoAdd.Ignore
public class ModalChoiceCard extends BaseCard {
    private final Runnable on_use_or_chosen;
    private final String passed_name;
    private final String passed_desc;

    public ModalChoiceCard(String name, String description, Runnable on_use_or_chosen) {
        super(name, -2, CardType.SKILL, CardRarity.SPECIAL, CardTarget.NONE, CardColor.COLORLESS);
        this.name = this.originalName = passed_name = name;
        this.rawDescription = passed_desc = description;
        this.on_use_or_chosen = on_use_or_chosen;
        initializeTitle();
        initializeDescription();
    }

    @Override
    public void onChoseThisOption() {
        on_use_or_chosen.run();
    }

    @Override
    public void use(AbstractPlayer abstractPlayer, AbstractMonster abstractMonster) {
        on_use_or_chosen.run();
    }

    @Override
    public void upp() { }

    @Override
    public boolean canUpgrade() {
        return false;
    }

    @Override
    public void upgrade() { }

    @Override
    public AbstractCard makeCopy() {
        return new ModalChoiceCard(passed_name, passed_desc, on_use_or_chosen);
    }
}
