package TheTodo.cards;

import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.monsters.AbstractMonster;

import static TheTodo.TodoMod.id;

public class Defend extends BaseCard {
    public final static String NAME = Defend.class.getSimpleName();
    public final static String ID = id(NAME);
    public final static CardType TYPE = CardType.SKILL;
    public final static CardTarget TARGET = CardTarget.SELF;
    public final static CardRarity RARITY = CardRarity.BASIC;

    public final static int COST = 1;
    public final static int BLOCK = 5;
    public final static int UP_BLOCK = 3;

    public Defend() {
        super(NAME, COST, TYPE, RARITY, TARGET);
        block(BLOCK);
    }

    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        block();
    }

    @Override
    public void upp() {
        upgradeBlock(UP_BLOCK);
    }
}