package TheTodo.relics;

import TheTodo.TodoCharacter;
import TheTodo.util.TexLoader;
import basemod.abstracts.CustomRelic;
import com.megacrit.cardcrawl.actions.common.RelicAboveCreatureAction;
import com.megacrit.cardcrawl.cards.AbstractCard.CardColor;

import static TheTodo.TodoMod.id;
import static TheTodo.TodoMod.relicPath;
import static TheTodo.util.Wiz.atb;
import static TheTodo.util.Wiz.player;

public abstract class BaseRelic extends CustomRelic {
    public CardColor color;

    public BaseRelic(String name, RelicTier tier, LandingSound sfx) {
        this(name, tier, sfx, TodoCharacter.Enums.TODO_COLOR);
    }

    public BaseRelic(String name, RelicTier tier, LandingSound sfx, CardColor color) {
        super(id(name), TexLoader.get_texture(relicPath(name + ".png")), tier, sfx);
        outlineImg = TexLoader.get_texture(relicPath("outlines/" + name + ".png"));
        this.color = color;
    }

    public void flash_above_player() {
        atb(new RelicAboveCreatureAction(player(), this));
    }

    @Override
    public String getUpdatedDescription() {
        return DESCRIPTIONS[0];
    }
}
