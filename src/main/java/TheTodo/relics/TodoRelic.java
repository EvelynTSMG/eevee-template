package TheTodo.relics;

import static TheTodo.TodoMod.id;

public class TodoRelic extends BaseRelic {
    public final static String NAME = TodoRelic.class.getSimpleName();
    public final static String ID = id(NAME);
    public final static RelicTier TIER = RelicTier.STARTER;
    public final static LandingSound SOUND = LandingSound.FLAT;

    public TodoRelic() {
        super(NAME, TIER, SOUND);
    }
}