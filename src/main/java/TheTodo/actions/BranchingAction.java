package TheTodo.actions;

import TheTodo.util.Wiz;
import com.megacrit.cardcrawl.actions.AbstractGameAction;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.cards.CardGroup;

import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.function.Supplier;

import static TheTodo.util.Wiz.att;

/**
 * Action that will run a {@code test},
 * then run {@code on_success} if it succeeded
 * or {@code on_failure} if it failed
 */
@SuppressWarnings("OptionalUsedAsFieldOrParameterType")
public class BranchingAction extends AbstractGameAction {
    public final Supplier<Boolean> test;
    private final AbstractGameAction on_success;
    private final Optional<AbstractGameAction> on_failure;

    public BranchingAction(Supplier<Boolean> test, AbstractGameAction on_success) {
        this(test, on_success, Optional.empty());
    }

    public BranchingAction(Supplier<Boolean> test, AbstractGameAction on_success, AbstractGameAction on_failure) {
        this(test, on_success, Optional.ofNullable(on_failure));
    }

    public BranchingAction(Supplier<Boolean> test, AbstractGameAction on_success, Optional<AbstractGameAction> on_failure) {
        this.test = test;
        this.on_success = on_success;
        this.on_failure = on_failure;
    }

    public void update() {
        if (test.get()) att(on_success);
        else on_failure.ifPresent(Wiz::att);

        isDone = true;
    }
}
